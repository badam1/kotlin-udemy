package com.bodansky.functions

/*
 * Created by Adam Bodansky on 2017.11.25..
 */

fun main(args: Array<String>) {
    println("this is all lowercase".upperFirstAndLast())
}

// "Extend" the String class with this function (just illusion)
fun String.upperFirstAndLast(): String {
    val upperFirst = substring(0, 1).toUpperCase() + substring(1)
    return upperFirst.substring(0, upperFirst.length - 1) + upperFirst.substring(upperFirst.length - 1, upperFirst.length).toUpperCase()
}