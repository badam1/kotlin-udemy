package com.bodansky.datatypes

/*
 * Created by Adam Bodansky on 2017.11.21..
 */
fun main(args: Array<String>) {

    var myInt = 10
    val myLong = 20L
    val myLong2: Long = 20

    myInt = myLong.toInt() // no auto widening data types

    var myDouble = 3.14
    println(myDouble is Double)

    val myFloat = 3.1428f

    println("this is a float ${myFloat is Float}")

    val char = 'b'

    println(65.toChar())

    val vacationTime = true
    println(DummyClass.isVacationType(vacationTime))

    val anything: Any // kotlin root type

    val unit: Unit // like void but it's exists it's a singleton

    /** everything can be nothing, like if you write a method which has an infinite loop,
    you tell the compiler it's never return so return Nothing */
    val nothing: Nothing

}