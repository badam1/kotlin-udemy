package com.bodansky.arrays

import com.bodansky.datatypes.DummyClass
import java.math.BigDecimal

/*
 * Created by Adam Bodansky on 2017.11.21..
 */
fun main(args: Array<String>) {

    val names = arrayOf("John", "Jane", "Joe", "Jill")

    val longs = arrayOf(1L, 2L, 3L)

    val longs1 = arrayOf<Long>(1, 2, 3)

    println(names[2])

    val evenNumbers = Array(16, { it * 2 })
    evenNumbers.forEach { println(it) }

    val lotsOfNumbers = 1..100000
    lotsOfNumbers.forEach { println(it) }

    var someArray: Array<Int>
    someArray = arrayOf(1, 2, 3, 4)
    for (number in someArray) println(number)


    someArray = Array(6, { i -> (i + 1) * 10 })
    for (n in someArray) println(n)

    val mixedArray = arrayOf("Hello", 10, BigDecimal(32), 'C')

    for (e in mixedArray) println(e)

    val myIntArray = intArrayOf(1, 2, 3, 4, 5, 6, 7)            // need to use primitive array types

    DummyClass.printNumbers(myIntArray)

    var someOtherIntArray = IntArray(5)                     // special arrays for primimtive types

    evenNumbers.toIntArray()
}