package com.bodansky.accessmodifiers

/*
 * Created by Adam Bodansky on 2017.11.22..
 */
fun main(args: Array<String>) {

    /*  - default visibility is public
        - classes and file names not have to match

        Top level modifiers:
            - private: Visible within the same file
            - public: Visible everywhere
            - internal: Visible within the same module (But from java it's public)
     */

    val emp = Employee()
    println(emp)
}

private class Employee