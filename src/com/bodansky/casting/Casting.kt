package com.bodansky.casting

import com.bodansky.equality.Employee

/*
 * Created by Adam Bodansky on 2017.11.21..
 */

fun main(args: Array<String>) {

    val something: Any = 13

    if (something is Employee) {
        // val employee = something as Employee // smart casting, not needed
        println(something.name)
    }
}