package com.bodansky.collections

/*
 * Created by Adam Bodansky on 2017.11.29..
 */

fun main(args: Array<String>) {

    // immutable but only in kotlin
    val strings = listOf("Spring", "Summer", "Fall", "Winter")
    println(strings.javaClass)

    val emptyList = emptyList<String>()
    println(emptyList.javaClass)

    val notNullList = listOfNotNull("hello", null, "goodbye")
    println(notNullList)

    val arrayList = arrayListOf(1, 2, 3)
    println(arrayList.javaClass)

    val mutableList = mutableListOf(1, 2, 3)
    println(mutableList.javaClass)

    println(mutableList[2])
    mutableList[1] = 20
    println(mutableList)

    val array = arrayOf("black", "white", "green")
    val colorList = listOf(*array)
    val colorList2 = array.toList()
    println(colorList)
    println(colorList2)

    val ints = intArrayOf(1, 2, 3)
    println(ints.toList())

}