package com.bodansky.objectkeyword

import java.time.Year

/*
 * Created by Adam Bodansky on 2017.11.26..
 */


fun main(args: Array<String>) {
    println(CompanyCommunications.getTagLine())
    println(CompanyCommunications.getCopyRightTagLine())
}

// Singleton
object CompanyCommunications {

    val currentYear = Year.now().value
    fun getTagLine() = "Our Company Rocks"
    fun getCopyRightTagLine() = "Copyright \u00a9 $currentYear Our company. All rights reserved."
}


