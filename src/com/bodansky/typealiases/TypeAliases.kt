package com.bodansky.typealiases

/*
 * Created by Adam Bodansky on 2017.11.21..
 */

// e.g. public typealias StringBuilder = java.lang.StringBuilder

typealias EmployeeSet = Set<Employee>

fun main(args: Array<String>) {

    var number: Int
    val number42 = 42
    number = 30

    val employees: EmployeeSet


    val employee1 = Employee("Lynn Jones", 500)
    employee1.name = "Lynn Smith"

    val employee2: Employee
    when (employee1.name) {
        "Jane" -> return
    }

    employee2 = if (number < number42) {
        Employee("Jane Smith", 400)
    } else {
        Employee("Mike Watson", 150)
    }

    println(employee2.name)

}

class Employee(var name: String, val id: Int)