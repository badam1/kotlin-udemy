package com.bodansky.collections

/*
 * Created by Adam Bodansky on 2017.11.29..
 */

fun main(args: Array<String>) {

    val immutableMap = mapOf(1 to Car("green", "Toyota", 2015),
            2 to Car("red", "Ford", 2016),
            3 to Car("silver", "honda", 2013))

    println(immutableMap.javaClass)
    println(immutableMap)

    val mutableMap = mutableMapOf("John's car" to Car("red", "Range Rover", 2010),
            "Jane's car" to Car("blue", "hyundai", 2013))

    println(mutableMap.javaClass)
    println(mutableMap)
    mutableMap.put("Mary's car", Car("red", "corvette", 2000))

    val pair = Pair(10, "ten")
    val (firstValue, secondValue) = pair
    println("$firstValue $secondValue")

    val car = Car("blue", "Corvette", 1959)
    val (color, model, year) = car
    println("$color $model $year")
}

// data class has these component functions implemented automatically
class Car(val color: String, val model: String, val year: Int) {
    operator fun component1() = color
    operator fun component2() = model
    operator fun component3() = year
}