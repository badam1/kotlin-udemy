package com.bodansky.sequences


/*
 * Created by Adam Bodansky on 2017.11.29..
 */

fun main(args: Array<String>) {

    // sequence like streams in java, but use only if collections are large!!!!!!

    val immutableMap = mapOf(1 to Car("green", "Toyota", 2015),
            2 to Car("red", "Ford", 2016),
            3 to Car("silver", "honda", 2013))

    println(immutableMap.asSequence().filter { it.value.model == "Ford" }.map { it.value.color })

    listOf("Joe", "Mary", "Jane").asSequence()
            .filter { println("filtering $it"); it[0] == 'J' }
            .map { println("mapping $it"); it.toUpperCase() }
            .find { it.endsWith('E') }

}

data class Car(val color: String, val model: String, val year: Int)



