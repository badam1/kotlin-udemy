package com.bodansky.forloop

/*
 * Created by Adam Bodansky on 2017.11.26..
 */

fun main(args: Array<String>) {

    // no classic for loop
    // kotlin for loops use ranges

    val range = 1..5 // inclusive
    val charRange = 'a'..'z'
    val stringRange = "abc".."xyz"

    val backwardsRange = 5.downTo(1)

    val stepRange = 0.rangeTo(10).step(2)


    println(3 in range)
    println('q' in charRange)
    println("ccc" in stringRange)
    println(5 in backwardsRange)
    println(3 in stepRange)

    for (n in stepRange) {
        println(n)
    }

    stepRange.forEach { println(it) }

    (1..100).step(2).forEach { println(it) }

    for (i in 1..10 step 2) {
        println(i)
    }

    for (i in 10 downTo 0 step 3) {
        println(i)
    }

    for (i in 1 until 10) {
        println(i)
    }

    val seasons = arrayOf("spring", "summer", "winter", "fall")
    for (s in seasons) {
        println(s)
    }

    val notInSeason = "something" !in seasons
    println(notInSeason)

    for (index in seasons.indices) {
        println("${seasons[index]} is season number $index")
    }
    seasons.forEach { println(it) }

    seasons.forEachIndexed { index, value -> println("Index is $index value is $value") }

    for (i in 1..3) {
        println("i = $i")
        jLoop@ for (j in 1..4) {
            println("j = $j")
            for (k in 5..10) {
                println("k = $k")
                if (k == 7) break@jLoop
            }
        }
    }
}