package com.bodansky.interfaces

/*
 * Created by Adam Bodansky on 2017.11.26..
 */

interface MyInterface {
    fun myFunction(param: Int): String

    // abstract variable
    val something: Int

    // concrete variable with initialed value
    val numberTwo: Int
        get() = 50
}

interface MySubInterface : MyInterface {
    fun mySubFunction(param: Int): String
}

class MyClass : MyClassTwo(), MySubInterface {

    override val something: Int = 25

    override fun myFunction(param: Int): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun mySubFunction(param: Int): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

open class MyClassTwo