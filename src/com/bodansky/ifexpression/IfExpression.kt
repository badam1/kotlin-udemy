package com.bodansky.ifexpression

/*
 * Created by Adam Bodansky on 2017.11.28..
 */

fun main(args: Array<String>) {

    // if statements are expressions

    val someCondition = 69 < 22
    val num = if (someCondition) 50 else 592

    val num2 = if (someCondition) {
        println("something")
        50
    } else {
        println("something else")
        5023
    }

    println("The result of the if expression is ${if (someCondition) {
        println("something")
        50
    } else {
        println("something else")
        5023
    }}")
    println(num2)
}