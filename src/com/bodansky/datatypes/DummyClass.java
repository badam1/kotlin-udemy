package com.bodansky.datatypes;

import java.util.Arrays;

/*
 * Created by Adam Bodansky on 2017.11.21..
 */
public class DummyClass {

    public static String isVacationType(boolean onVacation) {
        return onVacation ? "I'm on vacation" : "I'm working";
    }

    public static void printNumbers(int[] numbers) {
        Arrays.stream(numbers).forEach(System.out::println);
    }
}
