package com.bodansky.collections

/*
 * Created by Adam Bodansky on 2017.11.29..
 */

fun main(args: Array<String>) {

    val setInts = setOf(10, 15, 19, 5, 3, -22)

    println(setInts.filter { it % 2 != 0 })

    val immutableMap = mapOf(1 to Car("green", "Toyota", 2015),
            2 to Car("red", "Ford", 2016),
            3 to Car("silver", "honda", 2013))

    println(immutableMap.filter { it.value.year == 2016 })

    val mutableMap = mutableMapOf(1 to Car("green", "Toyota", 2015),
            2 to Car("red", "Ford", 2016),
            3 to Car("silver", "honda", 2013),
            4 to Car("black", "Ford", 2016),
            5 to Car("yellow", "audi", 2013))


    mutableMap.filter { it.value.color == "silver" }
    println("The filtered map is $mutableMap")

    val ints = arrayOf(1, 2, 3, 4, 5)
    val add10List = ints.map { it + 10 }
    println(add10List)

    val carYears = mutableMap.map { it.value.year }
    println(carYears)

    println(mutableMap.filter { it.value.model == "Ford" }.map { it.value.color })


    println(mutableMap.any { it.value.year > 2014 })

    println(mutableMap.all { it.value.year > 2014 })

    println(mutableMap.count { it.value.year > 2014 })

    println(mutableMap.values.find { it.year > 2014 })

    println(mutableMap.values.groupBy { it.color }.toSortedMap())

    val cars = mutableMap.values
    println(cars.sortedBy { it.year })

}