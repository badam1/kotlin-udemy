/*
 * Created by Adam Bodansky on 2017.11.26..
 */

fun main(args: Array<String>) {

    val simpleBike = Bicycle(30, 25, 3)
    val mountainBike = MountainBike(35, 40, 30, 11)
    val roadBike = RoadBike(30, 22, 44, 9)

    simpleBike.printDescription()
    mountainBike.printDescription()
    roadBike.printDescription()

    val simpleBike2 = Bicycle(12, 21)
    val mountainBike2 = MountainBike(13, 13, 12)
    val roadBike2 = RoadBike(11, 11, 11)

    simpleBike2.printDescription()
    mountainBike2.printDescription()
    roadBike2.printDescription()

    val mountainBike3 = MountainBike("blue", 11, 10, 10)
    mountainBike3.printDescription()

    println("The bike has available colors of: ${MountainBike.availableColors}")
}


open class Bicycle(var cadence: Int, private var speed: Int, var gear: Int = 10) {

    fun applyBreak(decrement: Int) {
        speed -= decrement
    }

    fun speedUp(increment: Int) {
        speed += increment
    }

    open fun printDescription() = println("Bike is in gear $gear with cadence $cadence travelling at speed $speed.")
}

class MountainBike(var seatHeight: Int, cadence: Int, speed: Int, gear: Int = 10) : Bicycle(cadence, speed, gear) {

    constructor(color: String, seatHeight: Int, cadence: Int, speed: Int, gear: Int = 20) : this(seatHeight, cadence, speed, gear) {
        println("The bike color is $color")
    }

    override fun printDescription() {
        super.printDescription()
        println("The mountain bike has a seat height of $seatHeight inches.")
    }

    companion object {
        val availableColors = listOf("blue", "red", "white", "black", "green", "brown")
    }
}

class RoadBike(val tireWidth: Int, cadence: Int, speed: Int, gear: Int = 10) : Bicycle(cadence, speed, gear) {
    override fun printDescription() {
        super.printDescription()
        println("The road bike has a tire width of $tireWidth mm.")
    }
}