package com.bodansky.nullreferences

/*
 * Created by Adam Bodansky on 2017.11.21..
 */

fun main(args: Array<String>) {

    val str: String? = "This is not null but could be"
    println(str?.toUpperCase())
    val str2: String? = null

    println("What happens if we do this: ${str2?.toUpperCase()}")

    val str3 = str2 ?: "default value"
    println(str3)

    val something: Any = arrayOf(1, 2, 3, 4)
    val str4 = something as? String     // safe cast operator
    println(str4)

    val strNotNull: String? = "This is not null!!"
    val str5 = strNotNull!!.toUpperCase()
    println(str5)

    str?.let { printText(it) } // if not null 'let' call this function

    val str7: String? = null
    val anotherString = "This is not nullable"
    println(str7 == anotherString)

    val nullableInts = arrayOfNulls<Int>(5) // make an array of nulls
    nullableInts.forEach { println(it) }
}

fun printText(text: String) = println(text)
