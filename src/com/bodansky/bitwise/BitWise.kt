package com.bodansky.bitwise

/*
 * Created by Adam Bodansky on 2017.11.21..
 */

fun main(args: Array<String>) {

    val x = 0x00101101
    val y = 0x11011011

    x or y // no | exists
    x and y // no & exists
}