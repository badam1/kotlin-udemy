package com.bodansky.collections

/*
 * Created by Adam Bodansky on 2017.11.29..
 */

fun main(args: Array<String>) {

    val setInts = setOf(10, 20, 30, 41, 5, -6)
    println(setInts.plus(22))
    println(setInts.plus(10))
    println(setInts.minus(5))

    println(setInts.average())

    println(setInts.drop(3))

    val mutableInts = mutableSetOf(1, 2, 3, 4, 5)
    mutableInts.plus(10)
    println(mutableInts)
    mutableInts.add(10)
    println(mutableInts)


}