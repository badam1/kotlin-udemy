package com.bodansky.inheritance

/*
 * Created by Adam Bodansky on 2017.11.25..
 */

fun main(args: Array<String>) {
    val laserPrinter = LaserPrinter("Brother 1234", 50)
    laserPrinter.printModel()
    println(laserPrinter.bestSellingPrize())
}

abstract class Printer(val modelName: String) {
    open fun printModel() = println("The model name of the printer is $modelName")
    abstract fun bestSellingPrize(): Double
}


open class LaserPrinter(modelName: String, ppm: Int) : Printer(modelName) {
    override fun bestSellingPrize() = 129.99
    final override fun printModel() = println("The model name of this laser printer is $modelName")
}

class SpecialLaserPrinter(modelName: String, ppm: Int) : LaserPrinter(modelName, ppm) {
    // not allowed cause it's final in LaserPrinter ---> override fun printModel() = println("This is the way i'm doing it")
}
