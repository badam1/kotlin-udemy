package com.bodansky.classes

/*
 * Adam Bodansky on 2017.11.22..
 */

val MY_CONSTANT = 100

fun main(args: Array<String>) {


    println(MY_CONSTANT)

    val emp = Employee("Adam", "Bodansky", false)
    println("${emp.firstName} ${emp.lastName} ${emp.fullTime}")

    println(Demo().dummy)

    emp.fullTime = false
    println(emp.fullTime)

}

class Employee(val firstName: String, val lastName: String, fullTime: Boolean = true) {

    // custom getter-setter need to be after de declaration, and can't be at the primary constructor
    var fullTime = fullTime
        get() {
            println("Running a custom get")
            return field
        }
        set(value) {
            println("Running a custom set")
            field = value
        }
}

// old way class with constructor
class Demo {
    val dummy: String

    constructor() {
        dummy = "Hello"
    }
}