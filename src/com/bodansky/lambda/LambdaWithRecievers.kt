package com.bodansky.lambda

/*
 * Created by Adam Bodansky on 2017.11.29..
 */

fun main(args: Array<String>) {

    val employees = listOf(
            Employee("John", "Smith", 2012),
            Employee("Jane", "Wilson", 2015),
            Employee("Mary", "Johnson", 2010),
            Employee("Mike", "Jones", 2002)
    )

//    println(countTo100())

    findByLastName(employees, "Wilson")
    findByLastName(employees, "Smithson")

    "Some String".apply someString@ {
        "ANOTHER STRING".apply {
            println(toLowerCase())
            println(this@someString.toUpperCase())
        }
    }
}

fun findByLastName(employees: List<Employee>, lastName: String) {
    // label lambda if we want a local return
    employees.forEach returnBLock@ {
        if (it.lastName == lastName) {
            println("Yes, there's an employee with the last name $lastName")
            return@returnBLock
        }
    }
    println("Nope, there's no employee with the last name $lastName")
}

fun countTo100() = StringBuilder().apply { (1..100).forEach { append(it).append(", ") } }

//fun countTo100() = with(StringBuilder()) {
//    (1..100).forEach { append(it).append(", ") }
//    toString()
//}


//fun countTo100(): String {
//    val numbers = StringBuilder()
//    for (i in 1..99) {
//        numbers.append(i)
//        numbers.append(", ")
//    }
//    numbers.append(100)
//    return numbers.toString()
//}