/*
 * Created by Adam Bodansky on 2017.11.28..
 */


fun main(args: Array<String>) {

//    // 5, 10, 15, 20, 25.... 5000
    (5..5000).step(5).forEach { println(it) }

    // -500 -> 0
    (-500..0).forEach { println(it) }

    // first 15 num of fibonacci numbers
    var total: Int
    var secondToLast = 0
    var last = 1

    println(secondToLast)
    println(last)
    (1..13).forEach {
        total = secondToLast + last
        secondToLast = last
        last = total
        println(total)
    }

    // 1, 11, 100, 99, 98, 2
    iLoop@ for (i in 1..5) {
        println(i)
        if (i == 2) break
        for (j in 11..20) {
            println(j)
            for (k in 100 downTo 90) {
                println(k)
                if (k == 98)
                    continue@iLoop
            }
        }
    }

    val num = 30
    val dNum: Double = when {
        num > 100 -> -234.567
        num < 100 -> 4444.555
        else -> 0.0
    }
    println(dNum)

}
