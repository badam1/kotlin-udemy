package com.bodansky.functions

/*
 * Created by Adam Bodansky on 2017.11.25..
 */

// inline functions body generated directly to the bytecode instead have in a function
inline fun labelMultiplyInline(operand1: Int, operand2: Int, label: String = "The Result is:") = "$label ${operand1 * operand2}"
