package com.bodansky.classes.dataclasses

/*
 * Created by Adam Bodansky on 2017.11.24..
 */


fun main(args: Array<String>) {

    val car = Car("blue", "Toyota", 2015)
    println(car)
    val car2 = Car("blue", "Toyota", 2015)
    println(car == car2)


    val car3 = car.copy(color = "red", year = 2017)
    println(car3)
}

/* - primary constructors need to have at least one parameter declared as val or var
   - can't be sealed or abstract
   - no declared properties
   - generated toString and EqualsHashCode method
   - copy function
   - if need custom toString or Equals-Hashcode you can override those
 */
data class Car(val color: String, val model: String, val year: Int)
