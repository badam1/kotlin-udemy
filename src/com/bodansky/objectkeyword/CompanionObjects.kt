package com.bodansky.objectkeyword

/*
 * Created by Adam Bodansky on 2017.11.26..
 */

fun main(args: Array<String>) {

    SomeClass.accessPrivateVar()
}

class SomeClass {

    // Inside everything is "static"
    companion object {
        private val privateVar = 6

        fun accessPrivateVar() = println("accessing privateVar $privateVar")
    }
}