package com.bodansky.trycatch

import java.lang.Integer.parseInt

/*
 * Created by Adam Bodansky on 2017.11.28..
 */

fun main(args: Array<String>) {

    println(getNumber("22"))

    println(getNumber("22.43") ?: "i can't print the result")

    notImplementedYet("string")

}

fun notImplementedYet(something: String): Nothing {
    throw IllegalArgumentException("Implement me!")
}

fun getNumber(str: String): Int? {
    return try {
        parseInt(str)
    } catch (e: NumberFormatException) {
        null
    } finally {
        println("I'm in the finally block")
    }
}