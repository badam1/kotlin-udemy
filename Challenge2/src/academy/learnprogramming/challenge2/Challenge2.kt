package academy.learnprogramming.challenge2

/*
 * Created by Adam Bodansky on 2017.11.21..
 */
fun main(args: Array<String>) {

    val nonNullableFloat1 = -3847.384F
    val nonNullableFloat2 = (-3847.384).toFloat()


    val nonNullableFloat3: Float? = -3847.384f
    val nonNullableFloat4: Float? = (-3847.384).toFloat()

    val nonNullableShorts = Array(5, { (it + 1).toShort() })
    nonNullableShorts.forEach { println(it) }

    println("---------------")

    val nullableInts = Array<Int?>(40, { (it * 5) + 5 })
    nullableInts.forEach { println(it) }

    DummyClass().method1(charArrayOf('a', 'b', 'C'))

    val x: String? = "I AM IN UPPERCASE"
    val y = x?.toLowerCase() ?: "I give up"
    println("y is $y")

    x?.let { println(it.toLowerCase().replace("am", "am not")) }

    val myNonNullVariable: Int? = null
    myNonNullVariable!!.toDouble()
}