package com.bodansky.enums

import com.bodansky.enums.Department.HR
import com.bodansky.enums.Department.SALES

/*
 * Created by Adam Bodansky on 2017.11.26..
 */

fun main(args: Array<String>) {

    println(HR.getDepthInfo())
    println(SALES.numEmployees)
}

enum class Department(val fullName: String, val numEmployees: Int) {
    HR("Human Resources", 5),
    IT("Information Technology", 10),
    ACCOUNTING("Accounting", 3),
    SALES("Sales", 23); // <-- semi colon required

    fun getDepthInfo() = "The $fullName department has $numEmployees employees"
}