package com.bodansky.imports

import com.bodansky.enums.Department.IT
import com.bodansky.functions.Car
import com.bodansky.functions.printColors as pc
import com.bodansky.functions.upperFirstAndLast as upperFAL
import com.bodansky.objectkeyword.CompanyCommunications as CC

/*
 * Created by Adam Bodansky on 2017.11.26..
 */


fun main(args: Array<String>) {
    pc(Car("Blue", "Toyota", 2017))
    CC.getCopyRightTagLine()
    println(IT.getDepthInfo())

    println("a string work with".upperFAL())
}