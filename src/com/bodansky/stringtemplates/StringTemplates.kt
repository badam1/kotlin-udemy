package com.bodansky.stringtemplates

import com.bodansky.equality.Employee

/*
 * Created by Adam Bodansky on 2017.11.21..
 */
fun main(args: Array<String>) {

    val employee = Employee("Adam", 210)
    println(employee)

    val change = 4.22

    println("To show value of change $$change")
    println("Your change is $change")
    println("Your name is ${employee.name}")

    val filePath = """c:\somedir\somedir2""" // now it's valid, no need for escaping called: raw srting

    val veryLongString = """string is started here and
                            |continues here and can
                            |continues here etc...""".trimMargin()

    println(veryLongString)


    val numerator = 10.99
    val denominator = 20.00
    println("The value of $numerator divided by $denominator is ${numerator / denominator}")
}