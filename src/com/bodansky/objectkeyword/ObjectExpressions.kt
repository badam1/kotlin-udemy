package com.bodansky.objectkeyword

/*
 * Created by Adam Bodansky on 2017.11.26..
 */

fun main(args: Array<String>) {

    var thisIsMutable = 45

    wantSomeInterface(object : SomeInterface {
        override fun mustImplement(num: Int): String {
            thisIsMutable++
            return "This is from mustImplement: ${num * 100}"
        }
    })

    println(thisIsMutable)
}


fun wantSomeInterface(si: SomeInterface) = println("Printing from wantSomeInterface ${si.mustImplement(22)}")

interface SomeInterface {
    fun mustImplement(num: Int): String
}