package com.bodansky.objectkeyword

/*
 * Created by Adam Bodansky on 2017.11.26..
 */

fun main(args: Array<String>) {

    val someThingInstance = Something.justAssigned("this is the string as is")
    val someThingInstance2 = Something.upperOrLowerCase("this is upper or lowercase", false)

    println(someThingInstance.someString)

    println(someThingInstance2.someString)
}

// factory pattern
class Something private constructor(val someString: String) {

    companion object {
        fun justAssigned(str: String) = Something(str)
        fun upperOrLowerCase(str: String, lowercase: Boolean): Something {

            return if (lowercase) {
                Something(str.toUpperCase())
            } else {
                Something(str.toUpperCase())
            }
        }
    }
}