package com.bodansky.collections

/*
 * Created by Adam Bodansky on 2017.11.29..
 */

fun main(args: Array<String>) {

    val strings = listOf("Spring", "Summer", "Fall", "Summer", "Winter")
    val colorList = listOf("black", "white", "red", "white", "red")

    val mutableSeasons = strings.toMutableList()
    mutableSeasons.add("someOtherSeason")
    println(mutableSeasons)

    println(strings.last())
    println(strings.asReversed())

    println(strings.getOrNull(5))

    val ints = listOf(1, 2, 3, 4, 5)
    println(ints.max())

    println(colorList.zip(strings))

    val mergeList = listOf(colorList, strings)
    println(mergeList)

    val combinedList = colorList + strings
    println(combinedList)

    val noDupsList = colorList.union(strings)
    println(noDupsList)

    val noDupsColors = colorList.distinct()
    println(noDupsColors)

}