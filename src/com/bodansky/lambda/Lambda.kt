package com.bodansky.lambda

/*
 * Created by Adam Bodansky on 2017.11.29..
 */

fun main(args: Array<String>) {

    // run { println("I'm in a lambda!") }

    val employees = listOf(
            Employee("John", "Smith", 2012),
            Employee("Jane", "Wilson", 2015),
            Employee("Mary", "Johnson", 2010),
            Employee("Mike", "Jones", 2002)
    )
    // member reference
    println(employees.minBy(Employee::starYear))

    run(::topLevel)
}

fun topLevel() = println("I'm in a function!")

fun useParameter(employees: List<Employee>, num: Int) {
    employees.forEach {
        println(it.firstName)
        // can't change
        println(num)
    }
}


data class Employee(val firstName: String, val lastName: String, val starYear: Int)