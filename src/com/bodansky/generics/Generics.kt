package com.bodansky.generics

import java.math.BigDecimal

/*
 * Created by Adam Bodansky on 2017.11.29..
 */


fun main(args: Array<String>) {

    val list = mutableListOf("Hello")
    list.add("another string")
    list.printCollection()


    val bdList = mutableListOf(BigDecimal(-33.34), BigDecimal(3505.99), BigDecimal(0.329))
    bdList.printCollection()
}

fun <T> List<T>.printCollection() {
    this.forEach { println(it) }
}