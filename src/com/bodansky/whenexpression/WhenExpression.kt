package com.bodansky.whenexpression

import com.bodansky.whenexpression.Season.*
import java.math.BigDecimal

/*
 * Created by Adam Bodansky on 2017.11.28..
 */

enum class Season {
    SPRING, SUMMER, FALL, WINTER
}

fun main(args: Array<String>) {

    val num = 100
    val num2 = -150

    when {
        num < num2 -> println("num is less then num2")
        num > num2 -> println("num is greater the num2")
        else -> println("num = num2")
    }

    val timeOfYear = WINTER
    val str = when (timeOfYear) {
        SPRING -> "Flowers are blooming"
        SUMMER -> "It's hot"
        FALL -> "It's raining"
        WINTER -> "It's snowing"
    }
    println(str)



    when (num) {
        100, 600 -> println("600")
        in 100..199 -> println("in range 100..199")
        200 -> println("200")
        300 -> println("300")
        else -> println("Doesn't match anything")
    }

    val y = 10

    when (num) {
        y + 80 -> println("90")
        y + 90 -> println("100")
        200 -> println("200")
        300 -> println("300")
        else -> println("Doesn't match anything")
    }

    val obj: Any = "I'm a string"
    val obj2: Any = BigDecimal(25.2)
    val obj3: Any = 45

    val something: Any = obj2

    val z = when (something) {
        is String -> println(something.toUpperCase())
        is BigDecimal -> println(something.remainder(BigDecimal(10.5)))
        is Int -> println("${something - 22}")
        else -> println("I have no idea what type this is (Unit)")
    }

    val x = when (something) {
        is String -> {
            println(something.toUpperCase())
            32
        }
        is BigDecimal -> {
            println(something.remainder(BigDecimal(10.5)))
            43
        }
        is Int -> {
            println("${something - 22}")
            -5
        }
        else -> {
            println("I have no idea what type this is (Unit)")
            16
        }
    }
    println(x)
}