package academy.learnprogramming.challenge2;

/*
 * Created by Adam Bodansky on 2017.11.21..
 */

public class DummyClass {

    public void method1(char[] charArray) {
        for (char c : charArray) {
            System.out.println(c);
        }
    }
}
