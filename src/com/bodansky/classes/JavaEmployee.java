package com.bodansky.classes;

/*
 * Created by Adam Bodansky on 2017.11.22..
 */
public class JavaEmployee {

    private final String firstName;
    private final String lastName;
    private final boolean fullTime;

    public JavaEmployee(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullTime = true;
    }

    public JavaEmployee(String firstName, String lastName, boolean fullTime) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullTime = fullTime;
    }
}
