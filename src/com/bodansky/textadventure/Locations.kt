package com.bodansky.textadventure

import java.io.File

/*
 * Created by Adam Bodansky on 2017.11.21..
 */

fun readLocationInfo(): Map<Int, Location> {
    val locations = mutableMapOf<Int, Location>()

    File("resources/locations_big.txt").reader().forEachLine {
        val (locationId, description) = it.split("`")
        val location = Location(locationId.toInt(), description)
        locations[location.locationId] = location
    }

    File("resources/directions_big.txt").reader().forEachLine {
        val (locationId, direction, destinationId) = it.split(",")
        locations[locationId.toInt()]?.addExit(direction, destinationId.toInt())
    }

    return locations
}
