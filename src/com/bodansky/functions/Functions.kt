package com.bodansky.functions

/*
 * Created by Adam Bodansky on 2017.11.24..
 */
fun main(args: Array<String>) {

    // named parameters, you can change the order
    println(labelMultiply(label = "The result is:", operand2 = 4, operand1 = 3))

    println(Employee("adam").upperCaseFirstName())

    val car1 = Car("Blue", "Toyota", 2015)
    val car2 = Car("Red", "Ferrari", 1967)

    printColors(car1, car2)

    val carArray = arrayOf(car1, car2)

    // spread operator * is to unpack an array
    printColors(*carArray)

    val moreCars = arrayOf(car1, car2)
    val car4 = car2.copy()

    val lotsOfCars = arrayOf(*carArray, *moreCars, car4)
    lotsOfCars.forEach { println(it) }
}


/*
*  functions with a block body curly braces
*  functions without curly braces have an expression body
*/
fun labelMultiply(operand1: Int, operand2: Int, label: String = "The Result is:") = "$label ${operand1 * operand2}"

class Employee(val firstName: String) {

    fun upperCaseFirstName() = firstName.toUpperCase()
}

fun printColors(vararg cars: Car) = cars.forEach { println(it.color) }


data class Car(val color: String, val model: String, val year: Int)