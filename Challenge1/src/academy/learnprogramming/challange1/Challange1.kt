package academy.learnprogramming.challange1

/*
 * Created by Adam Bodansky on 2017.11.21..
 */
fun main(args: Array<String>) {

    val hello1 = "Hello"
    val hello2 = "Hello"
    println(hello1 === hello2)
    println(hello1 == hello2)

    var num = 2988

    val text: Any = "The any type is the root of the kotlin class hierarchy"
    if (text is String) println(text.toUpperCase())

    println("""1   1
        1  11
        1 111
        11111""".trimMargin("1"))
}